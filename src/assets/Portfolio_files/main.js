(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/webpack/hot sync ^\\.\\/log$":
/*!*************************************************!*\
  !*** (webpack)/hot sync nonrecursive ^\.\/log$ ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./log": "./node_modules/webpack/hot/log.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/webpack/hot sync ^\\.\\/log$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/aboutMe/about-me/about-me.component.html":
/*!**********************************************************!*\
  !*** ./src/app/aboutMe/about-me/about-me.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"about-me\">\n    <div class=\"clr-row\">\n      <div class=\"clr-col-sm-12\">\n        <h2 class=\"text-center about-me-intro\">Here's a little about me:</h2>\n      </div>  \n    </div>\n</section>\n"

/***/ }),

/***/ "./src/app/aboutMe/about-me/about-me.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/aboutMe/about-me/about-me.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".about-me-intro {\n  padding: 20px 0px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy90c21pdGgvRG9jdW1lbnRzL3BlcnNvbmFsL3BvcnRmb2xpby9zcmMvYXBwL2Fib3V0TWUvYWJvdXQtbWUvYWJvdXQtbWUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2Fib3V0TWUvYWJvdXQtbWUvYWJvdXQtbWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYWJvdXQtbWUtaW50cm8ge1xuICAgIHBhZGRpbmc6IDIwcHggMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/aboutMe/about-me/about-me.component.ts":
/*!********************************************************!*\
  !*** ./src/app/aboutMe/about-me/about-me.component.ts ***!
  \********************************************************/
/*! exports provided: AboutMeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutMeComponent", function() { return AboutMeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AboutMeComponent = /** @class */ (function () {
    function AboutMeComponent() {
    }
    AboutMeComponent.prototype.ngOnInit = function () {
    };
    AboutMeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-about-me',
            template: __webpack_require__(/*! ./about-me.component.html */ "./src/app/aboutMe/about-me/about-me.component.html"),
            styles: [__webpack_require__(/*! ./about-me.component.scss */ "./src/app/aboutMe/about-me/about-me.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AboutMeComponent);
    return AboutMeComponent;
}());



/***/ }),

/***/ "./src/app/animations.ts":
/*!*******************************!*\
  !*** ./src/app/animations.ts ***!
  \*******************************/
/*! exports provided: slideInAnimation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideInAnimation", function() { return slideInAnimation; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

var slideInAnimation = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routeAnimations', [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('Contact => *', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter, :leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ position: 'fixed', width: '100%' }), { optional: true }),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(-100%)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }))
            ], { optional: true }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(100%)' }))
            ], { optional: true }),
        ])
    ]),
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('Home => *', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter, :leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ position: 'fixed', width: '100%' }), { optional: true }),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(100%)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }))
            ], { optional: true }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(-100%)' }))
            ], { optional: true }),
        ])
    ]),
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('About => Contact', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter, :leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ position: 'fixed', width: '100%' }), { optional: true }),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(100%)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }))
            ], { optional: true }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':leave', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(0%)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('0.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'translateX(-100%)' }))
            ], { optional: true }),
        ])
    ]),
]);


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _aboutMe_about_me_about_me_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./aboutMe/about-me/about-me.component */ "./src/app/aboutMe/about-me/about-me.component.ts");
/* harmony import */ var _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./page-not-found/page-not-found.component */ "./src/app/page-not-found/page-not-found.component.ts");
/* harmony import */ var _mySkills_root_root_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./mySkills/root/root.component */ "./src/app/mySkills/root/root.component.ts");






var routes = [
    { path: 'home', component: _mySkills_root_root_component__WEBPACK_IMPORTED_MODULE_5__["RootComponent"] },
    { path: 'about-me', component: _aboutMe_about_me_about_me_component__WEBPACK_IMPORTED_MODULE_3__["AboutMeComponent"] },
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: '**', component: _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_4__["PageNotFoundComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, {
                    enableTracing: false,
                })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<clr-main-container>\n    <div class=\"content-container\">\n      <main class=\"content-area\">\n          <router-outlet></router-outlet>\n      </main>\n      <nav id=\"sidenavagaion\" class=\"sidenav\" [clr-nav-level]=\"2\">\n        <section id=\"topbanner\">\n            <div class=\"clr-row clr-align-items-center\">\n                <div class=\"clr-col-sm-12\">\n                    <img class=\"profile-img\" src=\"../assets/profilepic.jpeg\" alt=\"\">\n                </div>\n                <div class=\"clr-col-sm-12 text-center text-center-sm\">\n                    <h3 class=\"intro-text\">Hey there, My name is Thomas.</h3>\n                    <h4 style=\"color: whitesmoke;\">And I'm junior fullstack web  developer</h4>\n                </div>\n            </div>\n        </section>\n\n        <section class=\"sidenav-content\">\n                <div class=\"clr-col-sm-12\">\n                        <hr>\n                    </div>\n                    <div class=\"clr-col-sm-12\">\n                            <hr>\n                        </div>\n            <a  class=\"nav-link active\">\n                Home / Portfolio:\n            </a>\n            <a  class=\"nav-link \">\n                A little about me:\n            </a>\n            <a  class=\"nav-link \">\n                Resume:\n            </a>\n            <a  class=\"nav-link \">\n              Contact me:\n            </a>\n        </section>\n      </nav>\n    </div>\n  </clr-main-container>     \n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#sidenavagaion {\n  background-color: #1e2a3a; }\n\n#topbanner {\n  padding: 10px 0px; }\n\n#topbanner .profile-img {\n    width: 50%;\n    height: 50%;\n    border-radius: 5%;\n    display: block;\n    margin: auto;\n    margin-top: 50px; }\n\n#topbanner .intro-text {\n    color: #a7dac2; }\n\n.sidenav {\n  border-right: none; }\n\n.sidenav .sidenav-content .nav-link {\n    color: white;\n    background-color: #1e2a3a; }\n\n.sidenav .sidenav-content > .nav-link.active {\n    background-color: #1e2a3a;\n    color: #a7dac2; }\n\n.sidenav .sidenav-content .nav-link:hover {\n    color: #a7dac2; }\n\n.content-area {\n  background-color: #111821; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy90c21pdGgvRG9jdW1lbnRzL3BlcnNvbmFsL3BvcnRmb2xpby9zcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsIi9Vc2Vycy90c21pdGgvRG9jdW1lbnRzL3BlcnNvbmFsL3BvcnRmb2xpby9zcmMvYXBwc3R5bGluZ3Muc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNBLHlCQ0h5QixFQUFBOztBRE16QjtFQUNJLGlCQUFpQixFQUFBOztBQURyQjtJQUdRLFVBQVU7SUFDVixXQUFXO0lBQ1gsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxZQUFZO0lBQ1osZ0JBQWdCLEVBQUE7O0FBUnhCO0lBV1EsY0NmNkIsRUFBQTs7QURrQnJDO0VBQ0ksa0JBQWtCLEVBQUE7O0FBRHRCO0lBSVksWUFBWTtJQUNaLHlCQ3pCYSxFQUFBOztBRG9CekI7SUFRWSx5QkM1QmE7SUQ2QmIsY0MzQnlCLEVBQUE7O0FEa0JyQztJQVlRLGNDOUI2QixFQUFBOztBRGtDckM7RUFDSSx5QkNwQ3NCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vYXBwc3R5bGluZ3Muc2Nzc1wiO1xuXG4jc2lkZW5hdmFnYWlvbntcbmJhY2tncm91bmQtY29sb3I6ICRkYXJrLWJhY2tncm91bmQ7XG59XG5cbiN0b3BiYW5uZXIge1xuICAgIHBhZGRpbmc6IDEwcHggMHB4O1xuICAgIC5wcm9maWxlLWltZyB7XG4gICAgICAgIHdpZHRoOiA1MCU7XG4gICAgICAgIGhlaWdodDogNTAlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1JTtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgICAgbWFyZ2luLXRvcDogNTBweDtcbiAgICB9XHRcbiAgICAuaW50cm8tdGV4dCB7XG4gICAgICAgIGNvbG9yOiAkbGlnaHQtZ3JlZW4tdGV4dDtcbiAgICB9XG59XG4uc2lkZW5hdiB7XG4gICAgYm9yZGVyLXJpZ2h0OiBub25lO1xuICAgIC5zaWRlbmF2LWNvbnRlbnR7XG4gICAgICAgIC5uYXYtbGluayB7XG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkZGFyay1iYWNrZ3JvdW5kO1xuICAgICAgICB9ICAgXG4gICAgICAgICA+Lm5hdi1saW5rLmFjdGl2ZSB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkZGFyay1iYWNrZ3JvdW5kO1xuICAgICAgICAgICAgY29sb3I6ICRsaWdodC1ncmVlbi10ZXh0O1xuICAgICAgICB9IFxuICAgICAgICAgLm5hdi1saW5rOmhvdmVye1xuICAgICAgICBjb2xvcjogJGxpZ2h0LWdyZWVuLXRleHQ7XG4gICAgICAgIH1cbiAgICB9XG59XG4uY29udGVudC1hcmVhIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkbGlnaHQtYmFja2dyb3VuZDtcbn1cblxuIiwiJGRhcmstYmFja2dyb3VuZDogIzFlMmEzYTtcbiRsaWdodC1iYWNrZ3JvdW5kOiAjMTExODIxO1xuJGxpZ2h0LWdyZWVuLXRleHQ6IHJnYigxNjcsIDIxOCwgMTk0KTtcbiR3aGl0ZTogd2hpdGU7Il19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./animations */ "./src/app/animations.ts");



var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'portfolio';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            animations: [
                // animation triggers go here
                _animations__WEBPACK_IMPORTED_MODULE_2__["slideInAnimation"]
            ],
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _clr_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @clr/angular */ "./node_modules/@clr/angular/fesm5/clr-angular.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _aboutMe_about_me_about_me_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./aboutMe/about-me/about-me.component */ "./src/app/aboutMe/about-me/about-me.component.ts");
/* harmony import */ var _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./page-not-found/page-not-found.component */ "./src/app/page-not-found/page-not-found.component.ts");
/* harmony import */ var _mySkills_my_skills_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./mySkills/my-skills.module */ "./src/app/mySkills/my-skills.module.ts");










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _aboutMe_about_me_about_me_component__WEBPACK_IMPORTED_MODULE_7__["AboutMeComponent"],
                _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_8__["PageNotFoundComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _clr_angular__WEBPACK_IMPORTED_MODULE_5__["ClarityModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
                _mySkills_my_skills_module__WEBPACK_IMPORTED_MODULE_9__["MySkillsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/mySkills/_Data/portfolioData.ts":
/*!*************************************************!*\
  !*** ./src/app/mySkills/_Data/portfolioData.ts ***!
  \*************************************************/
/*! exports provided: PORTFOLIOITEMS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PORTFOLIOITEMS", function() { return PORTFOLIOITEMS; });
var PORTFOLIOITEMS = [
    {
        id: 1,
        title: 'MarginBuddy.com.au',
        listOfSkills: ['../../../assets/angular.png', '../../../assets/dotnet.png', '../../assets/csharp.png'],
        description: ' ',
        photoUrl: '../../../assets/marginbuddy.png',
        subSkills: 'Fullstack Web Development'
    },
    {
        id: 1,
        title: 'Portfolio',
        listOfSkills: ['../../../assets/angular.png'],
        description: ' ',
        photoUrl: '../../../assets/profilepic.jpeg',
        subSkills: 'Front-End Development'
    },
    {
        id: 1,
        title: 'MarginBuddy.com.au',
        listOfSkills: [''],
        description: ' ',
        photoUrl: '../../../assets/marginbuddy.png',
        subSkills: 'Fullstack Web Development'
    }
];


/***/ }),

/***/ "./src/app/mySkills/_Data/skillData.ts":
/*!*********************************************!*\
  !*** ./src/app/mySkills/_Data/skillData.ts ***!
  \*********************************************/
/*! exports provided: SKILLDETAILS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SKILLDETAILS", function() { return SKILLDETAILS; });
var SKILLDETAILS = [
    {
        id: 1,
        title: 'Angular (V2 +)',
        photoUrl: '../../../assets/angular.png',
        listOfToolsAndSkillsUsed: ['Angular CLI', 'RXJS / Observables', 'TypeScript', 'Angular Material / Clarity Design']
    },
    {
        id: 2,
        title: 'HTML / CSS',
        photoUrl: '../../../assets/html5.png',
        listOfToolsAndSkillsUsed: ['SASS', 'Bootstrap 3/4', 'Flexbox', 'WCAG 2.0']
    },
    {
        id: 3,
        title: 'Vanilla Javascript',
        photoUrl: ' ',
        listOfToolsAndSkillsUsed: [' ', ' ']
    },
    {
        id: 4,
        title: 'ASP.NET (Core 2+)',
        photoUrl: '../../../assets/dotnet.png',
        listOfToolsAndSkillsUsed: ["Entity Frame Work Core", 'AutoMapper', "JWT's"]
    }
];


/***/ }),

/***/ "./src/app/mySkills/_Services/skills-service.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/mySkills/_Services/skills-service.service.ts ***!
  \**************************************************************/
/*! exports provided: SkillsServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SkillsServiceService", function() { return SkillsServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _Data_skillData__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_Data/skillData */ "./src/app/mySkills/_Data/skillData.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _Data_portfolioData__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_Data/portfolioData */ "./src/app/mySkills/_Data/portfolioData.ts");






var SkillsServiceService = /** @class */ (function () {
    function SkillsServiceService() {
    }
    SkillsServiceService.prototype.getSkills = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(_Data_skillData__WEBPACK_IMPORTED_MODULE_3__["SKILLDETAILS"]);
    };
    SkillsServiceService.prototype.getSkill = function (id) {
        return this.getSkills().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (skillDetails) { return skillDetails.find(function (skill) { return skill.id === +id; }); }));
    };
    SkillsServiceService.prototype.getPortfolioItems = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(_Data_portfolioData__WEBPACK_IMPORTED_MODULE_5__["PORTFOLIOITEMS"]);
    };
    SkillsServiceService.prototype.getAllPortfolio = function () {
        return this.getPortfolioItems().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (skillDetails) { return skillDetails; }));
    };
    SkillsServiceService.prototype.getPortfolioItem = function (id) {
        return this.getPortfolioItems().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (skillDetails) { return skillDetails.find(function (portfolio) { return portfolio.id === +id; }); }));
    };
    SkillsServiceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SkillsServiceService);
    return SkillsServiceService;
}());



/***/ }),

/***/ "./src/app/mySkills/detail-skill-view/detail-skill-view.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/mySkills/detail-skill-view/detail-skill-view.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"clr-row\">\n    <div class=\"clr-col-sm-12\">\n        <a [routerLink]=\"['/home', {outlets: {'skills': ['list']}}]\">\n            <button class=\"btn btn-primary\">Back to skills list</button>\n        </a>\n    </div>\n    <div class=\"clr-col-sm-12 skill-outter-box\">\n        <div class=\"clr-col-sm-12 clr-col-lg-3 clr-offset-lg-1\">\n            <img class=\"skill-img\" src={{skillDetail.photoUrl}} alt=\"\">\n        </div>\n        <div class=\"\">\n            <div class=\"clr-col-sm-12\">\n                <h2 class=\"skill-heading\">{{skillDetail.title}}</h2>\n            </div>\n            <p class=\"skill-used\">Skills Used:</p>\n            <ul>\n                <li class=\"skill-list\" *ngFor=\"let skill of skillDetail.listOfToolsAndSkillsUsed\">\n                    {{skill}}\n                </li>\n            </ul>\n        </div>\n    </div>\n    <div class=\"clr-col-sm-12\">\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/mySkills/detail-skill-view/detail-skill-view.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/mySkills/detail-skill-view/detail-skill-view.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".skill-outter-box {\n  padding: 40px 0px;\n  display: flex; }\n  .skill-outter-box .skill-heading {\n    color: #a7dac2; }\n  .skill-outter-box .skill-description {\n    width: 50%;\n    color: white; }\n  .skill-outter-box .skill-list {\n    color: #a7dac2; }\n  .skill-outter-box .skill-used {\n    color: white;\n    text-decoration: underline; }\n  .skill-text {\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy90c21pdGgvRG9jdW1lbnRzL3BlcnNvbmFsL3BvcnRmb2xpby9zcmMvYXBwL215U2tpbGxzL2RldGFpbC1za2lsbC12aWV3L2RldGFpbC1za2lsbC12aWV3LmNvbXBvbmVudC5zY3NzIiwiL1VzZXJzL3RzbWl0aC9Eb2N1bWVudHMvcGVyc29uYWwvcG9ydGZvbGlvL3NyYy9hcHBzdHlsaW5ncy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0ksaUJBQWlCO0VBQ2pCLGFBQWEsRUFBQTtFQUZqQjtJQUlRLGNDSjZCLEVBQUE7RURBckM7SUFPUSxVQUFVO0lBQ1YsWUFBWSxFQUFBO0VBUnBCO0lBV1EsY0NYNkIsRUFBQTtFREFyQztJQWNRLFlBQVk7SUFDWiwwQkFBMEIsRUFBQTtFQUlsQztFQUNJLFlBQVksRUFBQSIsImZpbGUiOiJzcmMvYXBwL215U2tpbGxzL2RldGFpbC1za2lsbC12aWV3L2RldGFpbC1za2lsbC12aWV3LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uL2FwcHN0eWxpbmdzLnNjc3NcIjtcblxuLnNraWxsLW91dHRlci1ib3gge1xuICAgIHBhZGRpbmc6IDQwcHggMHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgLnNraWxsLWhlYWRpbmcge1xuICAgICAgICBjb2xvcjogJGxpZ2h0LWdyZWVuLXRleHQ7O1xuICAgIH1cbiAgICAuc2tpbGwtZGVzY3JpcHRpb24ge1xuICAgICAgICB3aWR0aDogNTAlO1xuICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgfVxuICAgIC5za2lsbC1saXN0IHtcbiAgICAgICAgY29sb3I6ICRsaWdodC1ncmVlbi10ZXh0O1xuICAgIH1cbiAgICAuc2tpbGwtdXNlZCB7XG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gICAgfVxufVxuXG4uc2tpbGwtdGV4dCB7XG4gICAgY29sb3I6IHdoaXRlO1xufSIsIiRkYXJrLWJhY2tncm91bmQ6ICMxZTJhM2E7XG4kbGlnaHQtYmFja2dyb3VuZDogIzExMTgyMTtcbiRsaWdodC1ncmVlbi10ZXh0OiByZ2IoMTY3LCAyMTgsIDE5NCk7XG4kd2hpdGU6IHdoaXRlOyJdfQ== */"

/***/ }),

/***/ "./src/app/mySkills/detail-skill-view/detail-skill-view.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/mySkills/detail-skill-view/detail-skill-view.component.ts ***!
  \***************************************************************************/
/*! exports provided: DetailSkillViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailSkillViewComponent", function() { return DetailSkillViewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Services_skills_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_Services/skills-service.service */ "./src/app/mySkills/_Services/skills-service.service.ts");




var DetailSkillViewComponent = /** @class */ (function () {
    function DetailSkillViewComponent(route, router, skillsservice) {
        this.route = route;
        this.router = router;
        this.skillsservice = skillsservice;
    }
    DetailSkillViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.id = this.route.paramMap.subscribe(function (params) {
            _this.id = params.get('id');
            console.log(_this.id);
            _this.skillsservice.getSkill(_this.id).subscribe(function (skilld) {
                _this.skillDetail = skilld;
                console.log(_this.skillDetail);
            });
        });
    };
    DetailSkillViewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detail-skill-view',
            template: __webpack_require__(/*! ./detail-skill-view.component.html */ "./src/app/mySkills/detail-skill-view/detail-skill-view.component.html"),
            styles: [__webpack_require__(/*! ./detail-skill-view.component.scss */ "./src/app/mySkills/detail-skill-view/detail-skill-view.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _Services_skills_service_service__WEBPACK_IMPORTED_MODULE_3__["SkillsServiceService"]])
    ], DetailSkillViewComponent);
    return DetailSkillViewComponent;
}());



/***/ }),

/***/ "./src/app/mySkills/my-skills.module.ts":
/*!**********************************************!*\
  !*** ./src/app/mySkills/my-skills.module.ts ***!
  \**********************************************/
/*! exports provided: MySkillsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MySkillsModule", function() { return MySkillsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _mySkills_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mySkills-routing.module */ "./src/app/mySkills/mySkills-routing.module.ts");
/* harmony import */ var _myskills_list_my_work_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./myskills-list/my-work.component */ "./src/app/mySkills/myskills-list/my-work.component.ts");
/* harmony import */ var _clr_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @clr/angular */ "./node_modules/@clr/angular/fesm5/clr-angular.js");
/* harmony import */ var _detail_skill_view_detail_skill_view_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detail-skill-view/detail-skill-view.component */ "./src/app/mySkills/detail-skill-view/detail-skill-view.component.ts");
/* harmony import */ var _root_root_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./root/root.component */ "./src/app/mySkills/root/root.component.ts");
/* harmony import */ var _portfolio_portfolio_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./portfolio/portfolio.component */ "./src/app/mySkills/portfolio/portfolio.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _portfolio_list_portfolio_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./portfolio-list/portfolio-list.component */ "./src/app/mySkills/portfolio-list/portfolio-list.component.ts");











var MySkillsModule = /** @class */ (function () {
    function MySkillsModule() {
    }
    MySkillsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _clr_angular__WEBPACK_IMPORTED_MODULE_5__["ClarityModule"],
                _mySkills_routing_module__WEBPACK_IMPORTED_MODULE_3__["MySkillsRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_9__["BrowserAnimationsModule"]
            ],
            declarations: [
                _myskills_list_my_work_component__WEBPACK_IMPORTED_MODULE_4__["MyWorkComponent"],
                _detail_skill_view_detail_skill_view_component__WEBPACK_IMPORTED_MODULE_6__["DetailSkillViewComponent"],
                _root_root_component__WEBPACK_IMPORTED_MODULE_7__["RootComponent"],
                _portfolio_portfolio_component__WEBPACK_IMPORTED_MODULE_8__["PortfolioComponent"],
                _portfolio_list_portfolio_list_component__WEBPACK_IMPORTED_MODULE_10__["PortfolioListComponent"]
            ],
        })
    ], MySkillsModule);
    return MySkillsModule;
}());



/***/ }),

/***/ "./src/app/mySkills/mySkills-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/mySkills/mySkills-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: MySkillsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MySkillsRoutingModule", function() { return MySkillsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _myskills_list_my_work_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./myskills-list/my-work.component */ "./src/app/mySkills/myskills-list/my-work.component.ts");
/* harmony import */ var _detail_skill_view_detail_skill_view_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./detail-skill-view/detail-skill-view.component */ "./src/app/mySkills/detail-skill-view/detail-skill-view.component.ts");
/* harmony import */ var _root_root_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./root/root.component */ "./src/app/mySkills/root/root.component.ts");
/* harmony import */ var _portfolio_list_portfolio_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./portfolio-list/portfolio-list.component */ "./src/app/mySkills/portfolio-list/portfolio-list.component.ts");







var mySkillsRoutes = [
    { path: 'home', component: _root_root_component__WEBPACK_IMPORTED_MODULE_5__["RootComponent"],
        children: [
            { path: '', component: _myskills_list_my_work_component__WEBPACK_IMPORTED_MODULE_3__["MyWorkComponent"], outlet: 'skills', data: { animation: 'Home' } },
            { path: '', component: _portfolio_list_portfolio_list_component__WEBPACK_IMPORTED_MODULE_6__["PortfolioListComponent"], outlet: 'portfolio', data: { animation: 'Contact' } },
            { path: 'list', component: _myskills_list_my_work_component__WEBPACK_IMPORTED_MODULE_3__["MyWorkComponent"], outlet: 'skills', data: { animation: 'Contact' } },
            { path: ':id', component: _detail_skill_view_detail_skill_view_component__WEBPACK_IMPORTED_MODULE_4__["DetailSkillViewComponent"], outlet: 'skills', data: { animation: 'About' } },
        ] },
];
var MySkillsRoutingModule = /** @class */ (function () {
    function MySkillsRoutingModule() {
    }
    MySkillsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(mySkillsRoutes)
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
            ]
        })
    ], MySkillsRoutingModule);
    return MySkillsRoutingModule;
}());



/***/ }),

/***/ "./src/app/mySkills/myskills-list/my-work.component.html":
/*!***************************************************************!*\
  !*** ./src/app/mySkills/myskills-list/my-work.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"skill-intro\">\n  <div class=\"clr-row\">\n    <div class=\"clr-col-sm-12\">\n      <h2 class=\"my-skills-heading\">What I <span class=\"strike-through\">know:</span> pretend to know.. <span\n          class=\"click-me\">(click for more details)</span></h2>\n    </div>\n    <!--SKILL ICON-->\n    <div class=\"clr-col-sm-6 clr-offset-md-1 clr-col-md-3 skill-icon-outer \">\n      <a [routerLink]=\"['/home', {outlets: {'skills': [1]}}]\">\n        <div class=\"d-flex hvr-bob\">\n          <img class=\"skill-icon\" src=\"../../../assets/angular.png\" alt=\"\">\n          <h5 class=\"skill-heading\">Angular (2+)</h5>\n        </div>\n        <div class=\"\">\n        </div>\n      </a>\n    </div>\n    <!--SKILL ICON-->\n    <!--SKILL ICON-->\n    <div class=\"clr-col-sm-6 clr-col-md-3 skill-icon-outer\">\n      <a [routerLink]=\"['/home', {outlets: {'skills': [2]}}]\">\n        <div class=\"d-flex  hvr-bob \">\n          <div>\n            <img class=\"skill-icon\" src=\"../../../assets/html5.png\" alt=\"\">\n            <img class=\"skill-icon\" src=\"../../../assets/css.png\" alt=\"\">\n          </div>\n          <h5 class=\"skill-heading\">HTML / CSS</h5>\n        </div>\n      </a>\n\n    </div>\n    <!--SKILL ICON-->\n    <!--SKILL ICON-->\n    <div class=\"clr-col-sm-6 clr-col-md-3 skill-icon-outer \">\n      <a [routerLink]=\"['/home', {outlets: {'skills': [3]}}] \">\n\n        <div class=\"d-flex hvr-bob\">\n          <img class=\"skill-icon\" src=\"../../../assets/javascript.png\" alt=\"\">\n          <h5 class=\"skill-heading\">Vanilla JavaScript</h5>\n        </div>\n      </a>\n    </div>\n    <!--SKILL ICON-->\n\n    <!--SKILL ICON-->\n    <div class=\"clr-col-sm-6 clr-offset-md-1 clr-col-md-3 skill-icon-outer \">\n      <a [routerLink]=\"['/home', {outlets: {'skills': [4]}}] \">\n        <div class=\"d-flex hvr-bob\">\n          <img class=\"skill-icon\" src=\"../../../assets/dotnet.png\" alt=\"\">\n          <h5 class=\"skill-heading\">ASP.NET </h5>\n        </div>\n      </a>\n    </div>\n    <!--SKILL ICON-->\n    <!--SKILL ICON-->\n    <div class=\"clr-col-sm-6 clr-col-md-3 skill-icon-outer hvr-bob\">\n      <div class=\"d-flex\">\n        <img class=\"skill-icon\" src=\"../../../assets/csharp.png\" alt=\"\">\n        <h5 class=\"skill-heading\">C Sharp</h5>\n      </div>\n    </div>\n    <!--SKILL ICON-->\n    <!--SKILL ICON-->\n    <div class=\"clr-col-sm-6 clr-col-md-3 skill-icon-outer hvr-bob\">\n      <div class=\"d-flex\">\n        <img class=\"skill-icon\" src=\"../../../assets/mysql.svg\" alt=\"\">\n        <h5 class=\"skill-heading\"> SQL </h5>\n      </div>\n    </div>\n    <!--SKILL ICON-->\n  </div>\n</section>\n<!--END OF SKILL INTRO-->"

/***/ }),

/***/ "./src/app/mySkills/myskills-list/my-work.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/mySkills/myskills-list/my-work.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".my-skills-heading {\n  color: white; }\n\n.strike-through {\n  text-decoration: line-through; }\n\n.sub-skills-text {\n  padding-left: 50px;\n  text-decoration: underline;\n  color: white; }\n\n.b-flex {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  padding: 0px 60px; }\n\n.click-me {\n  font-size: 10px; }\n\n.skill-icon-outer {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  padding: 40px 0px; }\n\n.skill-icon-outer > a {\n    text-decoration: none; }\n\n.skill-icon-outer .skill-icon {\n    height: 30px;\n    width: 30px; }\n\n.skill-icon-outer .skill-heading {\n    margin-top: 10px;\n    color: #a7dac2; }\n\n.skill-icon-outer .d-flex {\n    display: flex;\n    flex-direction: column; }\n\n.skill-icon-outer .d-flex:hover {\n      cursor: pointer; }\n\n/* Bob */\n\n@-webkit-keyframes hvr-bob {\n  0% {\n    -webkit-transform: translateY(-8px);\n    transform: translateY(-8px); }\n  50% {\n    -webkit-transform: translateY(-4px);\n    transform: translateY(-4px); }\n  100% {\n    -webkit-transform: translateY(-8px);\n    transform: translateY(-8px); } }\n\n@keyframes hvr-bob {\n  0% {\n    -webkit-transform: translateY(-8px);\n    transform: translateY(-8px); }\n  50% {\n    -webkit-transform: translateY(-4px);\n    transform: translateY(-4px); }\n  100% {\n    -webkit-transform: translateY(-8px);\n    transform: translateY(-8px); } }\n\n@-webkit-keyframes hvr-bob-float {\n  100% {\n    -webkit-transform: translateY(-8px);\n    transform: translateY(-8px); } }\n\n@keyframes hvr-bob-float {\n  100% {\n    -webkit-transform: translateY(-8px);\n    transform: translateY(-8px); } }\n\n.hvr-bob {\n  display: inline-block;\n  vertical-align: middle;\n  -webkit-transform: perspective(1px) translateZ(0);\n  transform: perspective(1px) translateZ(0);\n  box-shadow: 0 0 1px rgba(0, 0, 0, 0); }\n\n.hvr-bob:hover, .hvr-bob:focus, .hvr-bob:active {\n  -webkit-animation-name: hvr-bob-float, hvr-bob;\n  animation-name: hvr-bob-float, hvr-bob;\n  -webkit-animation-duration: .3s, 1.5s;\n  animation-duration: .3s, 1.5s;\n  -webkit-animation-delay: 0s, .3s;\n  animation-delay: 0s, .3s;\n  -webkit-animation-timing-function: ease-out, ease-in-out;\n  animation-timing-function: ease-out, ease-in-out;\n  -webkit-animation-iteration-count: 1, infinite;\n  animation-iteration-count: 1, infinite;\n  -webkit-animation-fill-mode: forwards;\n  animation-fill-mode: forwards;\n  -webkit-animation-direction: normal, alternate;\n  animation-direction: normal, alternate; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy90c21pdGgvRG9jdW1lbnRzL3BlcnNvbmFsL3BvcnRmb2xpby9zcmMvYXBwL215U2tpbGxzL215c2tpbGxzLWxpc3QvbXktd29yay5jb21wb25lbnQuc2NzcyIsIi9Vc2Vycy90c21pdGgvRG9jdW1lbnRzL3BlcnNvbmFsL3BvcnRmb2xpby9zcmMvYXBwc3R5bGluZ3Muc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLFlDQVMsRUFBQTs7QURFYjtFQUNJLDZCQUE2QixFQUFBOztBQUVqQztFQUNJLGtCQUFrQjtFQUNsQiwwQkFBMEI7RUFDMUIsWUNSUyxFQUFBOztBRFViO0VBQ0ksYUFBYTtFQUNiLDZCQUE2QjtFQUM3QixtQkFBbUI7RUFDbkIsaUJBQWtCLEVBQUE7O0FBRXRCO0VBQ0UsZUFBZSxFQUFBOztBQUdqQjtFQUlJLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGlCQUFpQixFQUFBOztBQVByQjtJQUVJLHFCQUFxQixFQUFBOztBQUZ6QjtJQVNRLFlBQVk7SUFDWixXQUFXLEVBQUE7O0FBVm5CO0lBYVEsZ0JBQWdCO0lBQ2hCLGNDbkM2QixFQUFBOztBRHFCckM7SUFpQlEsYUFBYTtJQUNiLHNCQUFzQixFQUFBOztBQWxCOUI7TUFvQlUsZUFBZSxFQUFBOztBQUl6QixRQUFBOztBQUNBO0VBQ0k7SUFDRSxtQ0FBbUM7SUFDbkMsMkJBQTJCLEVBQUE7RUFFN0I7SUFDRSxtQ0FBbUM7SUFDbkMsMkJBQTJCLEVBQUE7RUFFN0I7SUFDRSxtQ0FBbUM7SUFDbkMsMkJBQTJCLEVBQUEsRUFBQTs7QUFHL0I7RUFDRTtJQUNFLG1DQUFtQztJQUNuQywyQkFBMkIsRUFBQTtFQUU3QjtJQUNFLG1DQUFtQztJQUNuQywyQkFBMkIsRUFBQTtFQUU3QjtJQUNFLG1DQUFtQztJQUNuQywyQkFBMkIsRUFBQSxFQUFBOztBQUcvQjtFQUNFO0lBQ0UsbUNBQW1DO0lBQ25DLDJCQUEyQixFQUFBLEVBQUE7O0FBRy9CO0VBQ0U7SUFDRSxtQ0FBbUM7SUFDbkMsMkJBQTJCLEVBQUEsRUFBQTs7QUFHL0I7RUFDRSxxQkFBcUI7RUFDckIsc0JBQXNCO0VBQ3RCLGlEQUFpRDtFQUNqRCx5Q0FBeUM7RUFDekMsb0NBQW9DLEVBQUE7O0FBRXRDO0VBQ0UsOENBQThDO0VBQzlDLHNDQUFzQztFQUN0QyxxQ0FBcUM7RUFDckMsNkJBQTZCO0VBQzdCLGdDQUFnQztFQUNoQyx3QkFBd0I7RUFDeEIsd0RBQXdEO0VBQ3hELGdEQUFnRDtFQUNoRCw4Q0FBOEM7RUFDOUMsc0NBQXNDO0VBQ3RDLHFDQUFxQztFQUNyQyw2QkFBNkI7RUFDN0IsOENBQThDO0VBQzlDLHNDQUFzQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbXlTa2lsbHMvbXlza2lsbHMtbGlzdC9teS13b3JrLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uL2FwcHN0eWxpbmdzLnNjc3NcIjtcblxuLm15LXNraWxscy1oZWFkaW5ne1xuICAgIGNvbG9yOiAkd2hpdGU7XG59XG4uc3RyaWtlLXRocm91Z2h7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XG59XG4uc3ViLXNraWxscy10ZXh0IHtcbiAgICBwYWRkaW5nLWxlZnQ6IDUwcHg7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gICAgY29sb3I6ICR3aGl0ZTtcbn1cbi5iLWZsZXgge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAgMHB4IDYwcHg7XG59XG4uY2xpY2stbWUge1xuICBmb250LXNpemU6IDEwcHg7XG59XG4vLyBTS0lMTCBJTUdTXG4uc2tpbGwtaWNvbi1vdXRlciB7XG4gID4gYSB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICB9XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmc6IDQwcHggMHB4O1xuICAgIC5za2lsbC1pY29uIHtcbiAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICB3aWR0aDogMzBweDtcbiAgICB9XG4gICAgLnNraWxsLWhlYWRpbmcge1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICBjb2xvcjogJGxpZ2h0LWdyZWVuLXRleHQ7XG4gICAgfVxuICAgIC5kLWZsZXgge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAmOmhvdmVyIHtcbiAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICB9XG4gICAgfVxufVxuLyogQm9iICovXG5ALXdlYmtpdC1rZXlmcmFtZXMgaHZyLWJvYiB7XG4gICAgMCUge1xuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLThweCk7XG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLThweCk7XG4gICAgfVxuICAgIDUwJSB7XG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNHB4KTtcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNHB4KTtcbiAgICB9XG4gICAgMTAwJSB7XG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtOHB4KTtcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtOHB4KTtcbiAgICB9XG4gIH1cbiAgQGtleWZyYW1lcyBodnItYm9iIHtcbiAgICAwJSB7XG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtOHB4KTtcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtOHB4KTtcbiAgICB9XG4gICAgNTAlIHtcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC00cHgpO1xuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC00cHgpO1xuICAgIH1cbiAgICAxMDAlIHtcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC04cHgpO1xuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC04cHgpO1xuICAgIH1cbiAgfVxuICBALXdlYmtpdC1rZXlmcmFtZXMgaHZyLWJvYi1mbG9hdCB7XG4gICAgMTAwJSB7XG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtOHB4KTtcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtOHB4KTtcbiAgICB9XG4gIH1cbiAgQGtleWZyYW1lcyBodnItYm9iLWZsb2F0IHtcbiAgICAxMDAlIHtcbiAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC04cHgpO1xuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC04cHgpO1xuICAgIH1cbiAgfVxuICAuaHZyLWJvYiB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHBlcnNwZWN0aXZlKDFweCkgdHJhbnNsYXRlWigwKTtcbiAgICB0cmFuc2Zvcm06IHBlcnNwZWN0aXZlKDFweCkgdHJhbnNsYXRlWigwKTtcbiAgICBib3gtc2hhZG93OiAwIDAgMXB4IHJnYmEoMCwgMCwgMCwgMCk7XG4gIH1cbiAgLmh2ci1ib2I6aG92ZXIsIC5odnItYm9iOmZvY3VzLCAuaHZyLWJvYjphY3RpdmUge1xuICAgIC13ZWJraXQtYW5pbWF0aW9uLW5hbWU6IGh2ci1ib2ItZmxvYXQsIGh2ci1ib2I7XG4gICAgYW5pbWF0aW9uLW5hbWU6IGh2ci1ib2ItZmxvYXQsIGh2ci1ib2I7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tZHVyYXRpb246IC4zcywgMS41cztcbiAgICBhbmltYXRpb24tZHVyYXRpb246IC4zcywgMS41cztcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1kZWxheTogMHMsIC4zcztcbiAgICBhbmltYXRpb24tZGVsYXk6IDBzLCAuM3M7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tdGltaW5nLWZ1bmN0aW9uOiBlYXNlLW91dCwgZWFzZS1pbi1vdXQ7XG4gICAgYW5pbWF0aW9uLXRpbWluZy1mdW5jdGlvbjogZWFzZS1vdXQsIGVhc2UtaW4tb3V0O1xuICAgIC13ZWJraXQtYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogMSwgaW5maW5pdGU7XG4gICAgYW5pbWF0aW9uLWl0ZXJhdGlvbi1jb3VudDogMSwgaW5maW5pdGU7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tZmlsbC1tb2RlOiBmb3J3YXJkcztcbiAgICBhbmltYXRpb24tZmlsbC1tb2RlOiBmb3J3YXJkcztcbiAgICAtd2Via2l0LWFuaW1hdGlvbi1kaXJlY3Rpb246IG5vcm1hbCwgYWx0ZXJuYXRlO1xuICAgIGFuaW1hdGlvbi1kaXJlY3Rpb246IG5vcm1hbCwgYWx0ZXJuYXRlO1xuICB9XG4gIFxuIiwiJGRhcmstYmFja2dyb3VuZDogIzFlMmEzYTtcbiRsaWdodC1iYWNrZ3JvdW5kOiAjMTExODIxO1xuJGxpZ2h0LWdyZWVuLXRleHQ6IHJnYigxNjcsIDIxOCwgMTk0KTtcbiR3aGl0ZTogd2hpdGU7Il19 */"

/***/ }),

/***/ "./src/app/mySkills/myskills-list/my-work.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/mySkills/myskills-list/my-work.component.ts ***!
  \*************************************************************/
/*! exports provided: MyWorkComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyWorkComponent", function() { return MyWorkComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MyWorkComponent = /** @class */ (function () {
    function MyWorkComponent() {
    }
    MyWorkComponent.prototype.ngOnInit = function () {
    };
    MyWorkComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-my-work',
            template: __webpack_require__(/*! ./my-work.component.html */ "./src/app/mySkills/myskills-list/my-work.component.html"),
            styles: [__webpack_require__(/*! ./my-work.component.scss */ "./src/app/mySkills/myskills-list/my-work.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MyWorkComponent);
    return MyWorkComponent;
}());



/***/ }),

/***/ "./src/app/mySkills/portfolio-list/portfolio-list.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/mySkills/portfolio-list/portfolio-list.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"clr-row\">\n  <div class=\"clr-col-sm-12\">\n    <h2 class=\"my-skills-heading\">My Portfolio:</h2>\n  </div>\n  <div>\n    <!--EACH PORTFOLIO ITEM-->\n  <div class=\"clr-col-sm-12 clr-col-md-10 clr-offset-md-1 portfolio-item-outter\" *ngFor=\"let skill of portfolioItems\">\n    <div>\n      <img class=\"portfolio-img\" src={{skill.photoUrl}} alt=\"\">\n    </div>\n    <div class=\"\">\n      <h3 class=\"portfolio-title\">{{skill.title}}</h3>\n        <p class=\"skill-used-text\">Skills Used:</p>\n        <div class=\"skillImgs-outer\">\n          <div src=\"\" alt=\"\" *ngFor=\"let skillimg of skill.listOfSkills\">\n            <img class=\"skill-img\" src=\"{{skillimg}}\" alt=\"\">\n          </div>\n        </div>\n        <p class=\"skill-used-text\">Description:</p>\n        <p style=\"width: 85%;  color: #b9a9a9;\">Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque natus cumque deserunt placeat explicabo, vitae maiores voluptatum cum, ducimus libero repellendus reiciendis praesentium reprehenderit eos illo repellat deleniti quibusdam mollitia.</p>\n      </div>\n  </div>\n</div>\n\n</div>"

/***/ }),

/***/ "./src/app/mySkills/portfolio-list/portfolio-list.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/mySkills/portfolio-list/portfolio-list.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".my-skills-heading {\n  color: #a7dac2; }\n\n.portfolio-item-outter {\n  padding-top: 20px;\n  display: flex;\n  align-items: center;\n  padding-bottom: 40px;\n  border-bottom: 1px dashed #405452; }\n\n.portfolio-item-outter .portfolio-title {\n    color: #a7dac2; }\n\n.portfolio-item-outter .portfolio-subtext {\n    color: white; }\n\n.portfolio-item-outter .portfolio-img {\n    display: block;\n    margin: auto;\n    margin-right: 100px;\n    width: 150px; }\n\n.portfolio-item-outter .skill-used-text {\n    color: white;\n    text-align: left;\n    text-decoration: underline;\n    margin: 0px 0px 10px 0px; }\n\n.portfolio-item-outter .skillImgs-outer {\n    display: flex; }\n\n.portfolio-item-outter .skill-img {\n    width: 30px;\n    margin-right: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy90c21pdGgvRG9jdW1lbnRzL3BlcnNvbmFsL3BvcnRmb2xpby9zcmMvYXBwL215U2tpbGxzL3BvcnRmb2xpby1saXN0L3BvcnRmb2xpby1saXN0LmNvbXBvbmVudC5zY3NzIiwiL1VzZXJzL3RzbWl0aC9Eb2N1bWVudHMvcGVyc29uYWwvcG9ydGZvbGlvL3NyYy9hcHBzdHlsaW5ncy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0ksY0NEaUMsRUFBQTs7QURHckM7RUFDSSxpQkFBaUI7RUFDakIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsaUNBQWlDLEVBQUE7O0FBTHJDO0lBT1EsY0NWNkIsRUFBQTs7QURHckM7SUFVUSxZQ1pLLEVBQUE7O0FERWI7SUFhUSxjQUFjO0lBQ2QsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixZQUFZLEVBQUE7O0FBaEJwQjtJQW1CUSxZQ3JCSztJRHNCTCxnQkFBZ0I7SUFDaEIsMEJBQTBCO0lBQzFCLHdCQUF3QixFQUFBOztBQXRCaEM7SUF5QlEsYUFBYSxFQUFBOztBQXpCckI7SUE0QlEsV0FBVztJQUNYLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbXlTa2lsbHMvcG9ydGZvbGlvLWxpc3QvcG9ydGZvbGlvLWxpc3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vLi4vYXBwc3R5bGluZ3Muc2Nzc1wiO1xuXG4ubXktc2tpbGxzLWhlYWRpbmd7XG4gICAgY29sb3I6ICRsaWdodC1ncmVlbi10ZXh0O1xufVxuLnBvcnRmb2xpby1pdGVtLW91dHRlciB7XG4gICAgcGFkZGluZy10b3A6IDIwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHBhZGRpbmctYm90dG9tOiA0MHB4O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBkYXNoZWQgIzQwNTQ1MjtcbiAgICAucG9ydGZvbGlvLXRpdGxlIHtcbiAgICAgICAgY29sb3I6ICRsaWdodC1ncmVlbi10ZXh0O1xuICAgIH1cbiAgICAucG9ydGZvbGlvLXN1YnRleHQge1xuICAgICAgICBjb2xvcjogJHdoaXRlO1xuICAgIH0gICBcbiAgICAucG9ydGZvbGlvLWltZyB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgIG1hcmdpbi1yaWdodDogMTAwcHg7XG4gICAgICAgIHdpZHRoOiAxNTBweDtcbiAgICB9XG4gICAgLnNraWxsLXVzZWQtdGV4dCB7XG4gICAgICAgIGNvbG9yOiAkd2hpdGU7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7ICAgICAgICBcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gICAgICAgIG1hcmdpbjogMHB4IDBweCAxMHB4IDBweDtcbiAgICB9XG4gICAgLnNraWxsSW1ncy1vdXRlciB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgfVxuICAgIC5za2lsbC1pbWcge1xuICAgICAgICB3aWR0aDogMzBweDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xuICAgIH1cbn0iLCIkZGFyay1iYWNrZ3JvdW5kOiAjMWUyYTNhO1xuJGxpZ2h0LWJhY2tncm91bmQ6ICMxMTE4MjE7XG4kbGlnaHQtZ3JlZW4tdGV4dDogcmdiKDE2NywgMjE4LCAxOTQpO1xuJHdoaXRlOiB3aGl0ZTsiXX0= */"

/***/ }),

/***/ "./src/app/mySkills/portfolio-list/portfolio-list.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/mySkills/portfolio-list/portfolio-list.component.ts ***!
  \*********************************************************************/
/*! exports provided: PortfolioListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortfolioListComponent", function() { return PortfolioListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_skills_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_Services/skills-service.service */ "./src/app/mySkills/_Services/skills-service.service.ts");



var PortfolioListComponent = /** @class */ (function () {
    function PortfolioListComponent(skillService) {
        this.skillService = skillService;
    }
    PortfolioListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.skillService.getPortfolioItems().subscribe(function (port) { return [
            _this.portfolioItems = port.map(function (x) { return x; })
        ]; });
        console.log(this.portfolioItems);
    };
    PortfolioListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-portfolio-list',
            template: __webpack_require__(/*! ./portfolio-list.component.html */ "./src/app/mySkills/portfolio-list/portfolio-list.component.html"),
            styles: [__webpack_require__(/*! ./portfolio-list.component.scss */ "./src/app/mySkills/portfolio-list/portfolio-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Services_skills_service_service__WEBPACK_IMPORTED_MODULE_2__["SkillsServiceService"]])
    ], PortfolioListComponent);
    return PortfolioListComponent;
}());



/***/ }),

/***/ "./src/app/mySkills/portfolio/portfolio.component.html":
/*!*************************************************************!*\
  !*** ./src/app/mySkills/portfolio/portfolio.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\n\n  <div class=\" portfolio-item-outer-div\" style=\"display: flex;\">\n    <div class=\"clr-col-sm-12 clr-col-md-3 clr-offset-md-1\">\n      <img class=\"portfolio-img\" src=\"../../../assets/marginbuddy.png\" alt=\"\">\n    </div>\n    <div class=\"clr-col-sm-12 clr-col-md-8\">\n      <h2 class=\"portfolio-heading\">Marginbuddy.com.au - <span style=\"color: white; font-size: 15px;\">(Fullstack Web Development)</span> </h2>\n    </div>\n    <div class=\"portfolio-skills-row\">\n      <p></p>\n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "./src/app/mySkills/portfolio/portfolio.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/mySkills/portfolio/portfolio.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".portfolio-item-outer-div {\n  padding-top: 50px; }\n  .portfolio-item-outer-div .portfolio-img {\n    width: 100%;\n    max-width: 250px; }\n  .portfolio-item-outer-div .portfolio-heading {\n    color: #a7dac2;\n    margin: 0px; }\n  .arrow {\n  justify-content: center;\n  align-items: center;\n  display: flex;\n  margin: 0;\n  color: white;\n  font-size: 50px;\n  padding-top: 19px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy90c21pdGgvRG9jdW1lbnRzL3BlcnNvbmFsL3BvcnRmb2xpby9zcmMvYXBwL215U2tpbGxzL3BvcnRmb2xpby9wb3J0Zm9saW8uY29tcG9uZW50LnNjc3MiLCIvVXNlcnMvdHNtaXRoL0RvY3VtZW50cy9wZXJzb25hbC9wb3J0Zm9saW8vc3JjL2FwcHN0eWxpbmdzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUE7RUFDSSxpQkFBaUIsRUFBQTtFQURyQjtJQUdRLFdBQVc7SUFDWCxnQkFBZ0IsRUFBQTtFQUp4QjtJQU9RLGNDVDZCO0lEVTdCLFdBQVcsRUFBQTtFQUluQjtFQUNJLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLFNBQVM7RUFDVCxZQUFZO0VBQ1osZUFBZTtFQUNmLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbXlTa2lsbHMvcG9ydGZvbGlvL3BvcnRmb2xpby5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi9hcHBzdHlsaW5ncy5zY3NzXCI7XG5cbi8vcG9ydGZvbGlvLWl0ZW0tb3V0ZXItZGl2XG5cbi5wb3J0Zm9saW8taXRlbS1vdXRlci1kaXYge1xuICAgIHBhZGRpbmctdG9wOiA1MHB4O1xuICAgIC5wb3J0Zm9saW8taW1nIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIG1heC13aWR0aDogMjUwcHg7XG4gICAgfVxuICAgIC5wb3J0Zm9saW8taGVhZGluZyB7XG4gICAgICAgIGNvbG9yOiAkbGlnaHQtZ3JlZW4tdGV4dDtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgfVxufVxuXG4uYXJyb3cge1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW46IDA7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICBwYWRkaW5nLXRvcDogMTlweDtcbn0iLCIkZGFyay1iYWNrZ3JvdW5kOiAjMWUyYTNhO1xuJGxpZ2h0LWJhY2tncm91bmQ6ICMxMTE4MjE7XG4kbGlnaHQtZ3JlZW4tdGV4dDogcmdiKDE2NywgMjE4LCAxOTQpO1xuJHdoaXRlOiB3aGl0ZTsiXX0= */"

/***/ }),

/***/ "./src/app/mySkills/portfolio/portfolio.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/mySkills/portfolio/portfolio.component.ts ***!
  \***********************************************************/
/*! exports provided: PortfolioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PortfolioComponent", function() { return PortfolioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PortfolioComponent = /** @class */ (function () {
    function PortfolioComponent() {
    }
    PortfolioComponent.prototype.ngOnInit = function () {
    };
    PortfolioComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-portfolio',
            template: __webpack_require__(/*! ./portfolio.component.html */ "./src/app/mySkills/portfolio/portfolio.component.html"),
            styles: [__webpack_require__(/*! ./portfolio.component.scss */ "./src/app/mySkills/portfolio/portfolio.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PortfolioComponent);
    return PortfolioComponent;
}());



/***/ }),

/***/ "./src/app/mySkills/root/root.component.html":
/*!***************************************************!*\
  !*** ./src/app/mySkills/root/root.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n    <div style=\"min-height: 400px;\" [@routeAnimations]=\"o && o.activatedRouteData \n    && o.activatedRouteData['animation']\">\n    <router-outlet #o=\"outlet\" name='skills'></router-outlet>\n\n</div>\n<div class=\"clr-col-sm-12 clr-col-lg-10 clr-offset-lg-1\">\n    <hr>\n</div>\n<router-outlet name='portfolio'></router-outlet>"

/***/ }),

/***/ "./src/app/mySkills/root/root.component.scss":
/*!***************************************************!*\
  !*** ./src/app/mySkills/root/root.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL215U2tpbGxzL3Jvb3Qvcm9vdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/mySkills/root/root.component.ts":
/*!*************************************************!*\
  !*** ./src/app/mySkills/root/root.component.ts ***!
  \*************************************************/
/*! exports provided: RootComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RootComponent", function() { return RootComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/animations */ "./src/app/animations.ts");



var RootComponent = /** @class */ (function () {
    function RootComponent() {
    }
    RootComponent.prototype.ngOnInit = function () {
    };
    RootComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./root.component.html */ "./src/app/mySkills/root/root.component.html"),
            animations: [src_app_animations__WEBPACK_IMPORTED_MODULE_2__["slideInAnimation"]],
            styles: [__webpack_require__(/*! ./root.component.scss */ "./src/app/mySkills/root/root.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], RootComponent);
    return RootComponent;
}());



/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.html":
/*!**************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  page-not-found works!\n</p>\n"

/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2Utbm90LWZvdW5kL3BhZ2Utbm90LWZvdW5kLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.ts":
/*!************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.ts ***!
  \************************************************************/
/*! exports provided: PageNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageNotFoundComponent", function() { return PageNotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page-not-found',
            template: __webpack_require__(/*! ./page-not-found.component.html */ "./src/app/page-not-found/page-not-found.component.html"),
            styles: [__webpack_require__(/*! ./page-not-found.component.scss */ "./src/app/page-not-found/page-not-found.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!************************************************************************************!*\
  !*** multi (webpack)-dev-server/client?http://0.0.0.0:0/sockjs-node ./src/main.ts ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/tsmith/Documents/personal/portfolio/node_modules/webpack-dev-server/client/index.js?http://0.0.0.0:0/sockjs-node */"./node_modules/webpack-dev-server/client/index.js?http://0.0.0.0:0/sockjs-node");
module.exports = __webpack_require__(/*! /Users/tsmith/Documents/personal/portfolio/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map