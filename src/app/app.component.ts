import { Component, HostListener, ElementRef, ViewChild, OnInit } from '@angular/core';
import { slideInAnimation } from './animations';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ slideInAnimation ]
})


export class AppComponent implements OnInit {
  @ViewChild('commandLine') nameField: ElementRef;
  form: FormGroup;
  errorCommand = false;
  commandError = '';
  constructor(private router: Router) {}

  ngOnInit() {
    this.form = new FormGroup({
      command: new FormControl('')
    });
    // focus on input on init (auto focus already set on form)
    this.nameField.nativeElement.focus();
  }
  onSubmit() {
    this.errorCommand = false;
    let Command = this.form.value.command;
    // clear all white space from string / lowercase
    Command = Command.replace(/\s+/g, '').toLowerCase();
    // navigate to /command
    this.router.navigateByUrl(Command).catch(() => {
      // if /command not found (unkown command)
      this.commandError = Command;
      this.errorCommand = true;
    });
    // new command line
    this.form.reset();
  }
  // focus on load / clicks
  @HostListener('window:focus', ['$event'])
  onFocus(): void {
      this.nameField.nativeElement.focus();
  }
  // close error
  closeButton() {
    this.errorCommand = false;
    this.nameField.nativeElement.focus();
  }
}
