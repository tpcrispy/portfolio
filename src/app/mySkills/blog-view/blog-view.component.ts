import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SkillsServiceService } from '../_Services/skills-service.service';
import { BlogServiceService } from 'src/app/_Services/blog-service.service';
import { skillDetails } from '../_Models/skillDetails';
import { SkillsBlogsDetails } from '../_Models/skillBlogs';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-blog-view',
  templateUrl: './blog-view.component.html',
  styleUrls: ['./blog-view.component.scss']
})
export class BlogViewComponent implements OnInit {

  backURL: any;
  ID: number;
  skillDetail: skillDetails;
  blogs: SkillsBlogsDetails[];
  swiperSetup2: SwiperConfigInterface;

  constructor(private activeRoutedService: ActivatedRoute,
              private skillService: SkillsServiceService,
              private blogService: BlogServiceService
  ) { }
  ngOnInit() {
    // get route ID
    this.ID = this.activeRoutedService.snapshot.params.id;
    // get skillDetail infomation
    this.skillService.getSkill(this.ID).subscribe(src => {
      this.skillDetail = src;
    });
    // get blogs for skills
    this.blogService.getAllBlogsBySkill(this.skillDetail.blogLabel).subscribe(src => {
      this.blogs = src;
      // SWIPER SETUP FOR PORTFOLIOs
      this.swiperSetup2 = {
        direction: 'horizontal',
        slidesPerView: 3,
        autoHeight: true,
        width: 1000,
        breakpoints: {
          420: {
            slidesPerView: 1,
            centeredSlides: true,
            spaceBetween: 20,
            autoHeight: true
          }
        },
        pagination: {
          el: '.swiper-pagination',
          type: 'bullets',
          clickable: true,
        }
      };
    });
    // get data from router
    this.activeRoutedService.data.subscribe(data => {
      // bind the router back button inside view
      switch (data.kind) {
        case 'fe-data':
          this.backURL = { outlets: { 'fe-skills': ['list'] } };
          break;
        case 'be-data':
          console.log('be-data');
          this.backURL = { outlets: { 'be-skills': ['list'] } };
      }
    });

  }
}

