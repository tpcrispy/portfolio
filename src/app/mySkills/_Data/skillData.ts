import {  skillDetails } from '../_Models/skillDetails';
import { BINDING_INDEX } from '@angular/core/src/render3/interfaces/view';

export const SKILLDETAILS: skillDetails[] = [
{
    id: 1,
    title: 'Angular (V2 +)',
    blogLabel: 'Angular',
    photoUrl: ['../../../assets/angular.png', '../../../assets/ts.png'],
    listOfToolsAndSkillsUsed: ['Angular CLI', 'RXJS / Observables', 'TypeScript', 'Angular Material / Clarity Design'],
    skill: 'fe-skill',
    url: {outlets: {'fe-skills': [1]}},

},
{
    id: 2,
    title: 'HTML / (S)CSS',
    blogLabel: 'HTML/CSS',
    photoUrl: ['../../../assets/html5.png', '../../../assets/css.png', '../../../assets/sass.png'],
    listOfToolsAndSkillsUsed: ['SASS', 'Bootstrap 3/4', 'Flexbox', 'WCAG 2.0'],
    skill: 'fe-skill',
    url: {outlets: {'fe-skills': [2]}},


},
{
    id: 3,
    blogLabel: 'Javascript',
    title: 'Javascript',
    photoUrl: ['../../../assets/javascript.png ', '../../../assets/jquery.png'],
    listOfToolsAndSkillsUsed: [' ', ' '],
    skill: 'fe-skill',
    url: {outlets: {'fe-skills': [3]}},

},
{
    id: 4,
    title: 'ASP.NET (Core 2+)',
    blogLabel: '.Net',
    photoUrl: ['../../../assets/dotnet.png'],
    listOfToolsAndSkillsUsed: ['Entity Frame Work Core',  'AutoMapper', 'JWT\'s'],
    skill: 'be-skill',
    url: {outlets: {'be-skills': [4]}},

},
{
    id: 5,
    title: 'C#',
    blogLabel: 'C#',
    photoUrl: ['../../../assets/csharp.png'],
    listOfToolsAndSkillsUsed: ['Entity Frame Work Core',  'AutoMapper', 'JWT\'s'],
    skill: 'be-skill',
    url: {outlets: {'be-skills': [5]}},

},
{
    id: 6,
    title: 'Dev Ops',
    blogLabel: '.Net',
    photoUrl: ['../../../assets/docker.png', '../../../assets/aws.png', '../../../assets/github.svg'],
    listOfToolsAndSkillsUsed: ['Entity Frame Work Core',  'AutoMapper', 'JWT\'s'],
    skill: 'be-skill',
    url: {outlets: {'be-skills': [6]}},

}
];
