import {  portfolioDetails } from '../_Models/portfolioDetails';
import { skillDetails } from '../_Models/skillDetails';

export const PORTFOLIOITEMS: portfolioDetails[] = [
    {
        id: 1,
        title: 'MarginBuddy.com.au',
        listOfSkills: [{
            photoSkill: '../../../assets/angular.png',
            toolsUsed: ['Angular Material', 'Stripe', 'JWTs', 'REST API consumption', ]
         }, {
             photoSkill: '../../../assets/dotnet.png',
             toolsUsed: ['Entity Framework', 'AutoMapper', 'JWTs', 'ASP.NET Identity', 'HangFire']
         },
         {
             photoSkill: '../../assets/csharp.png',
             toolsUsed: ['ASP.NET CORE']
         },
         {
             photoSkill: '../../assets/html5.png',
             toolsUsed: ['Bootstrap 4', 'SASS', 'WCAG 2.0']
         }
        ],
            //  '', '', '../../assets/sass.png'],
        // tslint:disable-next-line:max-line-length
        description: ' <br />  MarginBuddy is an online solution to the popular fundraising method of selling Football Margin Tickets. Fully operational, MarinBuddy allows users to create, share and join fundraisers online.' +
        // tslint:disable-next-line:max-line-length
        `<br /> <br /> Each week MarginBuddy automatically collects all payments via stripe from participants and emails them their unique football MarginTicket for that round`,
        photoUrl: '../../../assets/marginbuddy.png',
        subSkills: 'Fullstack Web Development'
    },
    {
        id: 1,
        title: 'Portfolio',
        listOfSkills: ['../../../assets/angular.png'],
        description: ' ',
        photoUrl: '../../../assets/profilepic.jpeg',
        subSkills: 'Front-End Development'
    },
    {
        id: 1,
        title: 'MarginBuddy.com.au',
        listOfSkills: [''],
        description: ' ',
        photoUrl: '../../../assets/marginbuddy.png',
        subSkills: 'Fullstack Web Development'
    }
];
