import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { skillDetails } from '../_Models/skillDetails';
import { SKILLDETAILS } from '../_Data/skillData';
import { map } from 'rxjs/operators';
import { portfolioDetails } from '../_Models/portfolioDetails';
import { PORTFOLIOITEMS } from '../_Data/portfolioData';

@Injectable({
  providedIn: 'root'
})
export class SkillsServiceService {

  constructor() { }

  getSkills(): Observable<skillDetails[]> {
    return of(SKILLDETAILS);
  }
  getSkill(id: number) {
    return this.getSkills().pipe(
    map((SkillDetails: skillDetails[]) => SkillDetails.find(skill => skill.id === +id ))
    );
  }
  getSkillSet( skillset: string) {
    return this.getSkills().pipe(
    map((SkillDetails: skillDetails[]) => SkillDetails.filter(skill => skill.skill === skillset ))
    );
  }
  getPortfolioItems(): Observable<portfolioDetails[]> {
    return of(PORTFOLIOITEMS);
  }

  getAllPortfolio() {
    return this.getPortfolioItems().pipe(
      map((SkillDetails: portfolioDetails[]) => SkillDetails));
  }

  getPortfolioItem(id: number | string) {
    return this.getPortfolioItems().pipe(
      map((SkillDetails: portfolioDetails[]) => SkillDetails.find(portfolio => portfolio.id === +id))
      );
  }
}
