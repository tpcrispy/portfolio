export class SkillsBlogsDetails {
  url: string;
  title: string;
  updated: string;
  labels: string[];
}
