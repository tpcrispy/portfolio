// tslint:disable-next-line:class-name
export class portfolioDetails {
    id: number;
    title: string;
    photoUrl: string;
    description: string;
    listOfSkills: {}[];
    subSkills: string;
}
