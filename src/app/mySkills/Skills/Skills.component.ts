import { Component, OnInit } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { SkillsServiceService } from '../_Services/skills-service.service';
import { pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { TestBed } from '@angular/core/testing';
import { skillDetails } from '../_Models/skillDetails';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-front-end-skills',
  templateUrl: './front-end-skills.component.html',
  styleUrls: ['./front-end-skills.component.scss']
})
export class SkillsComponent implements OnInit {
  constructor(
    private skillService: SkillsServiceService,
    private activeRoutedService: ActivatedRoute
  ) { }
  swiperSetup: SwiperConfigInterface;
  skillsArray: [];
  skillend: string;
  swiperSetup2: SwiperConfigInterface;
  ngOnInit() {
    // SWIPER SETUP FOR PORTFOLIOs
    this.swiperSetup2 = {
      direction: 'horizontal',
      slidesPerView: 3,
      breakpoints: {
        445: {
          slidesPerView: 1.2,
          allowSlidePrev: true,
        }
      },
    };
    this.activeRoutedService.data.subscribe(data => {
      this.skillService.getSkillSet(data.kind).subscribe((src: any) => {
        this.skillsArray = src;
        if (data.kind === 'fe-skill') {
          this.skillend = 'Front-End Skills:';
        } else {
          this.skillend = 'Back-End Skills:';
        }
      });
    });
  }
}
