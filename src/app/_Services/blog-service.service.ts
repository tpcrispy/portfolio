import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SkillsBlogsDetails } from '../mySkills/_Models/skillBlogs';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class BlogServiceService {
  blogDTO: SkillsBlogsDetails[];
  APIKEY = environment.bloggerApi;
  constructor(private http: HttpClient) { }
  getAllBlogsBySkill(skillLabel: string): Observable<SkillsBlogsDetails[]  > {
    // tslint:disable-next-line:prefer-const
    let blogDTO =  [];
    // tslint:disable-next-line:whitespace
    return this.http.get<any>('https://www.googleapis.com/blogger/v3/blogs/7936430184808049883/posts?key='+this.APIKEY)
    .pipe(
      map( response => {
        response.items.forEach((element: SkillsBlogsDetails) => {
          const blogItem = new SkillsBlogsDetails();
          blogItem.title = element.title;
          blogItem.url = element.url;
          blogItem.labels = element.labels;
          // check if blog has label for skill
          blogItem.labels.forEach(skill => {
            if (skill === skillLabel) {
              blogDTO.push(blogItem);
            }
          });
        });
        return blogDTO;
      }),
    );
  }
}

