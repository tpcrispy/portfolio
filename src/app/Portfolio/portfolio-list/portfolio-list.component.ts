import { Component, OnInit } from '@angular/core';
import { SkillsServiceService } from '../../mySkills/_Services/skills-service.service';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { BlogServiceService } from 'src/app/_Services/blog-service.service';

@Component({
  selector: 'app-portfolio-list',
  templateUrl: './portfolio-list.component.html',
  styleUrls: ['./portfolio-list.component.scss']
})
export class PortfolioListComponent implements OnInit {
  // VARS
  swiperSetup: SwiperConfigInterface;
  portfolioItems: any;
  constructor(private skillService: SkillsServiceService) { }

  ngOnInit() {
    this.skillService.getPortfolioItems().subscribe(port => [
      this.portfolioItems = port.map(x => x)
    ]);
    // SWIPER SETUP FOR PORTFOLIO
    this.swiperSetup = {
      direction: 'horizontal',
      slidesPerView: 1,
      pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
        clickable: true,
      }
    };
  }


}
