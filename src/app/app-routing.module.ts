import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutMeComponent } from './aboutMe/about-me/about-me.component';
import { PortfolioListComponent } from './Portfolio/portfolio-list/portfolio-list.component';
import { ContactMeComponent } from './Contact/contact-me/contact-me.component';
import { SkillsComponent } from './mySkills/Skills/Skills.component';
import { BlogViewComponent } from './mySkills/blog-view/blog-view.component';
import { ResumeComponent } from './Resume/resume/resume.component';
import { RootComponent } from './mySkills/root/root.component';


const routes: Routes = [
  {path: 'about', component: AboutMeComponent},
  {path: 'portfolio', component: PortfolioListComponent},
  {path: 'contact', component: ContactMeComponent},
  {path: 'resume', component: ResumeComponent},
  // list of skills /lists
  {path: 'list', component: SkillsComponent,  outlet: 'fe-skills', data: {kind: 'fe-skill'}},
  {path: 'list', component: SkillsComponent,  outlet: 'be-skills', data: {kind: 'be-skill'}},
   // blog posts of skills
  {path: ':id', component: BlogViewComponent,  outlet: 'fe-skills', data: {kind: 'fe-data'}},
  {path: ':id', component: BlogViewComponent,  outlet: 'be-skills', data: {kind: 'be-data'}},
   // list of skills on load
   {path: 'skills', component: RootComponent,
   children: [
     // list of skills /lists
    {path: 'list', component: SkillsComponent,  outlet: 'fe-skills', data: {kind: 'fe-skill'}},
    {path: 'list', component: SkillsComponent,  outlet: 'be-skills', data: {kind: 'be-skill'}},
    // blog posts of skills
    {path: ':id', component: BlogViewComponent,  outlet: 'fe-skills', data: {kind: 'fe-data'}},
    {path: ':id', component: BlogViewComponent,  outlet: 'be-skills', data: {kind: 'be-data'}},
    // list of skills on load
    {path: '', component: SkillsComponent, outlet: 'fe-skills', data: {kind: 'fe-skill'}},
    {path: '', component: SkillsComponent, outlet: 'be-skills',  data: {kind: 'be-skill'}},
    // portfolio
  ]},
  {path: 'cls',  redirectTo: '/',  pathMatch: 'full'},
  {path: 'clear',  redirectTo: '/',  pathMatch: 'full'},
  { path: '', redirectTo: '/',  pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    {
      enableTracing: false, // <-- debugging purposes only
    }
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
