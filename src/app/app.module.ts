import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AboutMeComponent } from './aboutMe/about-me/about-me.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ResumeComponent } from './Resume/resume/resume.component';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PortfolioComponent } from './Portfolio/portfolio/portfolio.component';
import { SkillsComponent } from './mySkills/Skills/Skills.component';
import { BlogViewComponent } from './mySkills/blog-view/blog-view.component';
import { RouterModule } from '@angular/router';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { AppRoutingModule } from './app-routing.module';
import { PortfolioListComponent } from './Portfolio/portfolio-list/portfolio-list.component';
import { ContactMeComponent } from './Contact/contact-me/contact-me.component';
import { RootComponent } from './mySkills/root/root.component';
@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ResumeComponent,
    PortfolioComponent,
    SkillsComponent,
    BlogViewComponent,
    AboutMeComponent,
    PortfolioListComponent,
    ContactMeComponent,
    RootComponent
  ],
  imports: [
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    BrowserModule,
    ClarityModule,
    PdfJsViewerModule,
    BrowserAnimationsModule,
    PdfViewerModule,
    HttpClientModule,
    SwiperModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
